# Smallest multiple
# Problem 5
# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
#
# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
#
# we dont have to check 1,2,4,5,10 because they are factors of 20.
# similarly we can ignore 3,6,7,8, and 9 because they are built into the higher multples.

num = 20
i = False

while i != 1:
    if num % 19 == 0:
        if num % 18 == 0:
            if num % 17 == 0:
                if num % 16 == 0:
                    if num % 15 == 0:
                        if num % 14 == 0:
                            if num % 13 == 0:
                                if num % 12 == 0:
                                    if num % 11 == 0:
                                        print(num)
                                        i = 1
    num += 20

    # Yields 232792560
