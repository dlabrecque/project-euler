# Amicable numbers
# Problem 21
# Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
# If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable
# numbers.
#
# For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284.
# The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
#
# Evaluate the sum of all the amicable numbers under 10000.


def divisors(num):
    res = []
    for i in range(1, (num // 2) + 1):
        if not num % i:
            res.append(i)
    return res

divisor_dict = {1: 1}
for j in range(2, 10000):
    divisor_dict[j] = sum(divisors(j))
# divisor_dict = {k:divisor_dict[k] for k in divisor_dict if divisor_dict[k] != 1}

for x in divisor_dict.keys():
    if divisor_dict[x] < len(divisor_dict):
        if divisor_dict[divisor_dict[x]] == x and x != divisor_dict[x]:
            print(x, divisor_dict[x])

# this messy piece of shit yields:
# 220 284
# 284 220
# 1184 1210
# 1210 1184
# 2620 2924
# 2924 2620
# 5020 5564
# 5564 5020
# 6232 6368
# 6368 6232
#
# with sum of 31626


