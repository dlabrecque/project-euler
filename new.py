from time import time
from collections import defaultdict


def get_collatz():
    col_dict = defaultdict(int)
    n = 50
    for x in range(1, n):
        col_num = x
        count = 0
        if x not in col_dict:
            while True:
                if col_num in col_dict or col_num == 1:
                    count = col_dict[col_num] + count
                    col_dict[x] = count
                    y = 2 * x
                    while y <= n:
                        print(y, count)
                        count += 1
                        col_dict[y] = count
                        y *= 2
                    break
                if col_num % 2:
                    col_num = col_num + (col_num >> 1) + 1
                    count += 1
                else:
                    col_num = col_num >> 1
                count += 1
    return col_dict


start = time()
new = get_collatz()
end = time()
print(end - start)
